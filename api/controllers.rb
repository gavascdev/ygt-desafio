require 'json'

JSON_PATH = '../sorted.json'

def with_params?(params)
  return false if params.empty?

  params['page'].to_i if params.key?('page') && params['page'].to_i.positive?
end

def get_json_data
  JSON.parse(File.read(JSON_PATH))
end

def numbers_all
  get_json_data
end

def numbers_by_id(id)
  numbers = get_json_data
  numbers.empty? ? false : numbers[id]
end
