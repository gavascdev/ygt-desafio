require 'sinatra'
require 'json'
require_relative './controllers'

get '/numbers' do
  page = with_params?(params)
  limit = 100
  offset = (limit * page) - 1 if page

  numbers = numbers_all
  numbers = !page ? numbers : numbers[offset..offset + limit]
  # Ruby returns 'nil' if requesting a range outside of the array's length. ex: a[4..7] from a=[1,2,3]
  return JSON.dump({ 'total': 0, 'numbers': [] }) if numbers.nil?

  response = numbers.empty? ? { 'error': 'Data not available yet' } :
  {
    'total': numbers.length,
    'numbers': numbers
  }

  JSON.dump(response)
end

get '/numbers/:id' do |id|
  return JSON.dump({ 'error': 'Invalid id number' }) if id.to_i.negative?

  number = numbers_by_id(id.to_i)
  response = number ? { 'id': id.to_i, 'number': number } : { 'error': 'Number not found from given id' }

  JSON.dump(response)
end
