# Running the extractor
cd ./extractor
screen -S extractor -dm bash -c 'bundle install; ruby extractor.rb'

# Running the server
cd ../api
screen -S api -dm bash -c 'bundle install; ruby server.rb'

# Running the final sorter
cd ../final_sorter
screen -S final_sorter -dm bash -c 'ruby final_sorter.rb'
