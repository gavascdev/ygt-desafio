require 'json'

JSON_PATH = '../data.json'
SORTED_ARRAY_PATH = '../sorted.json'

def get_json
  JSON.parse(File.read(JSON_PATH))
end

def increment(new_array, value, index)
  new_array << value
  index += 1

  [new_array, index]
end

def merge(arr1, arr2)
  i = 0
  j = 0

  new_array = []
  while i < arr1.length || j < arr2.length
    first_value = arr1[i]
    second_value = arr2[j]
    # When one of the indexes reaches the end of the array its because all the elements
    # of the other array are lesser than its own elements
    if first_value.nil?
      new_array = [*new_array, *arr2[j..arr2.length]]
      break
    elsif second_value.nil?
      new_array = [*new_array, *arr1[i..arr1.length]]
      break
    end

    if first_value < second_value
      new_array, i = increment(new_array, first_value, i)
    else
      new_array, j = increment(new_array, second_value, j)
    end
  end
  new_array
end

def sort_all(arrays)
  new_array = []
  (1..arrays.length - 1).each do |i|
    aux_array = merge(arrays[0].dup, arrays[i].dup)
    arrays[0] = aux_array
    new_array = aux_array
  end
  new_array
end

# Main
loop do
  puts 'Still sorting arrays'
  sleep(10)
  json_data = get_json

  break if json_data['finished']
end

arrays = get_json['arrays']
sorted_array = sort_all(arrays)
File.write(SORTED_ARRAY_PATH, JSON.dump(sorted_array))
