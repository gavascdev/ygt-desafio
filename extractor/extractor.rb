require 'httparty'
require 'json'

JSON_PATH = '../data.json'

# Implementation of insertion sort
def sort(array)
  (1..array.length - 1).each do |i|
    j = i - 1
    curr = array[i]
    while j >= 0 && array[j] > curr
      array[j + 1] = array[j]
      j -= 1
    end
    array[j + 1] = curr
  end

  array
end

def get_numbers_array(page)
  HTTParty.get("http://challenge.dienekes.com.br/api/numbers?page=#{page}").parsed_response
end

def add_new_array(new_array, json_data)
  json_data['arrays'] << new_array
  json_data['page'] += 1
end

def get_json
  JSON.parse(File.read(JSON_PATH))
end

def update_json(json_data)
  File.write(JSON_PATH, JSON.dump(json_data))
end

# Main
loop do
  json_data = get_json
  new_array = get_numbers_array(json_data['page'])['numbers']
  if new_array.nil?
    redo
  elsif new_array.empty?
    json_data['finished'] = true
    update_json(json_data)
    break
  end

  sorted_array = sort(new_array.dup)
  add_new_array(sorted_array, json_data)
  update_json(json_data)
end
