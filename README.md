# ETL Pipeline como desafio da Cross Commerce
Este app foi desenvolvido como um desafio da Cross Commerce no processo seletivo para o cargo de Dev Backend Elixir Jr. A implementação foi feita em Ruby em todas as etapas, utilizando também a framework Sinatra para a construção da API.

## Funcionamento
**E**xtract -> A aplicação itera pelas páginas da API disponibilizada até encontrar o array vazio, o final dos registros. Cada array capturado é ordenado diretamente após a extração utilizando um algoritmo de Insertion Sort.

**T**ransform -> Após a extração de todos os arrays contidos na API a aplicação ordena todos os elementos em um único array. Como todos os arrays já estão ordenados entre si, a aplicação apenas faz um merge de cada array ordenado em um único array final.

**L**oad -> Quando o array ordenado final é completado a API implementada com Sinatra disponibiliza os seguintes endpoints:
  - GET _/numbers_: Disponibiliza todos os números presentes no arquivo final. Há a possibilidade de paginação, adicionando o paramentro '_?page=< n >_'. Cada página apresenta 100 números, assim como na API disponibilizada.
  - GET _/numbers/:id_: Retorna um único numero, com o índice no array final igual ao < id > passado.

## Como executar
O app foi enviado com os dados já disponíveis e ordenados, restando apenas rodar a API para vizualização dos mesmos. Caso seja requerido ver o app em funcionamento o scripts em _bash_ estão disponíveis.

Primeiro os dados devem ser resetados com o script _reset.sh_
```bash
> bash reset.sh
```

Com os dados resetados iremos iniciar os serviços com o script _run.sh_
```bash
> bash run.sh
```

Esse último cria três _screens_ separadas para cada serviço da aplicação, possibilitando que os três rodem simultaneamente.

## Problemas encontrados
O serviço _final\_sorter_ pode encerrar inesperadamente se estiver executando há muito tempo antes que todos os arrays sejam extraídos. Caso aconteça, apenas rode-o separadamente quando o _extractor_ terminar sua execucação e ele ordenará todos os números normalmente.

A solução do _extractor_ se torna um pouco ineficiente quando já foram capturados muitos arrays, pois o arquivo inteiro é carregado para que apenas mais um array seja carregado nele. Infelizmente não estou tendo muito tempo disponível essa semana para poder pensar e implementar uma solução melhor por agora.
